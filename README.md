# Installation

This plugins is based on the qbehaviour_studentfeedbackdeferred plugin, and needs it to be installed.

# About the Student feedback (immediate) behaviour

This question behaviour is derived from the Immediate feedback behaviour, and questions will behave essentially the same.

With this behaviour however, students can submit one comment for each question after it is submitted and one comment for the whole Quiz, after their attempt is submitted.  
These comments are editable by the student, and are visible by the teachers.

# Setup

Once installed, simply go to a Quiz settings > Question behaviour > Select "Student feedback (immediate)".

# Quiz report

Students feedback can be viewed within the quiz report interface using the quiz_feedback plugin.

# About

This software was developped with the Caseine project, with the support of the following organizations:  
- Université Grenoble Alpes  
- Institut Polytechnique de Grenoble
